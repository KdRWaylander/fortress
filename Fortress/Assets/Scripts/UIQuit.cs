﻿using UnityEngine;

public class UIQuit : MonoBehaviour
{
    public void Quit()
    {
        Application.Quit();
    }
}