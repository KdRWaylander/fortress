﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TerrainGenerator : MonoBehaviour
{
    [SerializeField] private int m_PixelResolutionPerTile = 10;

    [Header("General map")]
    private Texture2D m_OccupationTexture;
    [SerializeField] private float m_PerlinNoiseScaleGeneral = 3.0f;
    [SerializeField] private AnimationCurve m_GeneralAnimationCurve;
    [SerializeField] private Gradient m_GeneralColorGradient;

    [Header("Forest map")]
    private Texture2D m_ForestTexture;
    [SerializeField] private float m_PerlinNoiseScaleForest = 3.0f;
    [SerializeField] private AnimationCurve m_ForestAnimationCurve;
    [SerializeField] private Gradient m_ForestColorGradient;

    private float[,] m_Grid;

    private Color m_WaterColor = new Color(0f, 0f, 1f);
    private Color m_GrassColor = new Color(77f/255f, 202f/255f, 60f/255f);
    private Color m_ForestColor = new Color(0f, 167f/255f, 15f/255f);
    private Color m_MountainColor = new Color(173f / 255f, 79f / 255f, 9f / 255f);

    public void ComputeTextureMap(int gridWith, int gridHeight)
    {
        int texWidth = m_PixelResolutionPerTile * gridWith;
        int texHeight = m_PixelResolutionPerTile * gridHeight;
        m_OccupationTexture = new Texture2D(texWidth, texHeight);
        m_ForestTexture = new Texture2D(texWidth, texHeight);

        Vector3 offsetGeneral = new Vector3(Random.Range(100, 9999), 0, Random.Range(100, 9999));
        Vector3 offsetForest = new Vector3(Random.Range(100, 9999), 0, Random.Range(100, 9999));
        for (int z = 0; z < texHeight; z++)
        {
            for(int x = 0; x < texWidth; x++)
            {
                float xPerlinGeneral = offsetGeneral.x + (float)x / (float)texWidth * m_PerlinNoiseScaleGeneral;
                float yPerlinGeneral = offsetGeneral.z + (float)z / (float)texHeight * m_PerlinNoiseScaleGeneral;
                float sampleGeneral = Mathf.PerlinNoise(xPerlinGeneral, yPerlinGeneral);
                m_OccupationTexture.SetPixel(x, z, m_GeneralColorGradient.Evaluate(m_GeneralAnimationCurve.Evaluate(sampleGeneral)));

                float xPerlinForest = offsetForest.x + (float)x / (float)texWidth * m_PerlinNoiseScaleForest;
                float yPerlinForest = offsetForest.z + (float)z / (float)texHeight * m_PerlinNoiseScaleForest;
                float sampleForest = Mathf.PerlinNoise(xPerlinForest, yPerlinForest);
                m_ForestTexture.SetPixel(x, z, m_ForestColorGradient.Evaluate(m_ForestAnimationCurve.Evaluate(sampleForest)));
            }
        }
        m_OccupationTexture.Apply();
        m_ForestTexture.Apply();

        //FindObjectOfType<DebugPerlinMaps>().ShowMap(Sprite.Create(m_OccupationTexture, new Rect(0, 0, texWidth, texHeight), Vector2.zero), Map.General);
        //FindObjectOfType<DebugPerlinMaps>().ShowMap(Sprite.Create(m_ForestTexture, new Rect(0, 0, texWidth, texHeight), Vector2.zero), Map.Forest);
    }

    public TileOccupation GetPropType(int xGrid, int zGrid, Vector3 position)
    {
        int xTex = Mathf.FloorToInt((xGrid + .5f + position.x) * m_PixelResolutionPerTile);
        int zTex = Mathf.FloorToInt((zGrid + .5f + position.z) * m_PixelResolutionPerTile);
        Color forestTexColor = m_ForestTexture.GetPixel(xTex, zTex);
        Color generalTexColor = m_OccupationTexture.GetPixel(xTex, zTex);

        if (generalTexColor == m_WaterColor || generalTexColor == m_MountainColor)
        {
            return TileOccupation.Water;
        }

        return (forestTexColor == m_GrassColor) ? TileOccupation.Grass : TileOccupation.Forest;
    }

    public TileOccupation GetTileOccupation(int xGrid, int zGrid)
    {
        int xTex = m_PixelResolutionPerTile * xGrid + m_PixelResolutionPerTile / 2;
        int zTex = m_PixelResolutionPerTile * zGrid + m_PixelResolutionPerTile / 2;
        Color colorTex = m_OccupationTexture.GetPixel(xTex, zTex);

        if(colorTex == m_WaterColor)
            return TileOccupation.Water;
        else if(colorTex == m_MountainColor)
            return TileOccupation.Mountain;
        else
            return TileOccupation.Grass;
    }

    public TileOccupation GetTileOccupation(Vector3 position)
    {
        return GetTileOccupation((int)position.x, (int)position.z);
    }

    public List<PropLocation> GetPropLocations(int xGrid, int zGrid, int maxCount = 5, float minimalDistance = .25f)
    {
        List<PropLocation> list = new List<PropLocation>();

        while (list.Count < maxCount)
        {
            float x = Random.Range(-.5f, .5f);
            float z = Random.Range(-.5f, .5f);
            Vector3 position = new Vector3(x, .1f, z);

            if(list.Count <= 0)
            {
                list.Add(new PropLocation(position, GetPropType(xGrid, zGrid, position)));
            }
            else
            {
                bool isTooClose = false;
                for(int i = 0; i < list.Count; i++)
                {
                    if(Vector3.Distance(list[i].position, position) < minimalDistance)
                        isTooClose = true;
                }

                if(isTooClose == false)
                    list.Add(new PropLocation(position, GetPropType(xGrid, zGrid, position)));
            }
        }

        return list;
    }
}