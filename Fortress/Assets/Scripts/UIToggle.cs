﻿using System.Collections;
using UnityEngine;

public class UIToggle : MonoBehaviour
{
    [SerializeField] private GameObject m_Canvas;
    [SerializeField] private float m_ToggleTime = 3f;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && m_Canvas.activeSelf == false)
        {
            StartCoroutine(DisplayCanvas());
        }
    }

    private IEnumerator DisplayCanvas()
    {
        m_Canvas.SetActive(true);

        float elapsed = 0f;
        while(elapsed < m_ToggleTime)
        {
            elapsed += Time.deltaTime;
            yield return null;

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                m_Canvas.SetActive(false);
                StopAllCoroutines();
            }
        }

        m_Canvas.SetActive(false);
    }
}