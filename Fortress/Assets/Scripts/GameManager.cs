﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public BuildMode CurrentBuildMode { get; set; }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        CurrentBuildMode = BuildMode.Build;
    }
}

public enum BuildMode
{
    Build,
    Edit,
    Destroy
}