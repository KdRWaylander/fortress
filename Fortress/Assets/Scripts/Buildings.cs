﻿using UnityEngine;

[CreateAssetMenu(fileName = "New buildings", menuName = "Buildings")]
public class Buildings : ScriptableObject
{
    public BuildingType Wall;
    public BuildingType Tower;
    public BuildingType Tower1Connector;
    public BuildingType Tower2ConnectorsStraight;
    public BuildingType Tower2ConnectorsAngle;
    public BuildingType Tower3Connectors;
    public BuildingType Tower4Connectors;
    public BuildingType Door;
}