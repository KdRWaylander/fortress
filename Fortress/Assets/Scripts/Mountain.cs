﻿using UnityEngine;

public class Mountain : MonoBehaviour
{
    [SerializeField] private Buildings m_Mountains;

    private void Start()
    {
        Quaternion rotation = Quaternion.identity;
        GameObject prefab = GetMountainPrefab(transform.parent.GetComponent<Tile>(), out rotation);

        GameObject mountain = Instantiate(prefab, transform);
        mountain.transform.localRotation = rotation;
    }

    private GameObject GetMountainPrefab(Tile tile, out Quaternion rotation)
    {
        // Get local references to shorten code
        TerrainManager terrainManager = FindObjectOfType<TerrainManager>();
        TerrainGenerator terrainGenerator = FindObjectOfType<TerrainGenerator>();

        // Compute all posible neighbours positions since all neighbours are possibly not all already instantiated
        Vector3 northPosition = tile.position + Vector3.forward;
        Vector3 eastPosition = tile.position + Vector3.right;
        Vector3 southPosition = tile.position + Vector3.back;
        Vector3 westPosition = tile.position + Vector3.left;

        // Get configuration directly from occupation texture instead of asking to neighbours
        int configuration = 0;
        if (terrainManager.PositionIsValid(northPosition) && terrainGenerator.GetTileOccupation(northPosition) == TileOccupation.Mountain)
            configuration += 1000;
        if (terrainManager.PositionIsValid(eastPosition) && terrainGenerator.GetTileOccupation(eastPosition) == TileOccupation.Mountain)
            configuration += 100;
        if (terrainManager.PositionIsValid(southPosition) && terrainGenerator.GetTileOccupation(southPosition) == TileOccupation.Mountain)
            configuration += 10;
        if (terrainManager.PositionIsValid(westPosition) && terrainGenerator.GetTileOccupation(westPosition) == TileOccupation.Mountain)
            configuration += 1;

        // return prefab and rotation
        switch (configuration)
        {
            case 0:
                rotation = Quaternion.identity;
                return m_Mountains.Tower.prefab;
            case 1:
                rotation = Quaternion.Euler(0, 270, 0);
                return m_Mountains.Tower1Connector.prefab;
            case 10:
                rotation = Quaternion.Euler(0, 180, 0);
                return m_Mountains.Tower1Connector.prefab;
            case 11:
                rotation = Quaternion.Euler(0, 180, 0);
                return m_Mountains.Tower2ConnectorsAngle.prefab;
            case 100:
                rotation = Quaternion.Euler(0, 90, 0);
                return m_Mountains.Tower1Connector.prefab;
            case 101:
                rotation = Quaternion.Euler(0, 90, 0);
                return m_Mountains.Wall.prefab;
            case 110:
                rotation = Quaternion.Euler(0, 90, 0);
                return m_Mountains.Tower2ConnectorsAngle.prefab;
            case 111:
                rotation = Quaternion.Euler(0, 90, 0);
                return m_Mountains.Tower3Connectors.prefab;
            case 1000:
                rotation = Quaternion.identity;
                return m_Mountains.Tower1Connector.prefab;
            case 1001:
                rotation = Quaternion.Euler(0, 270, 0);
                return m_Mountains.Tower2ConnectorsAngle.prefab;
            case 1010:
                rotation = Quaternion.identity;
                return m_Mountains.Wall.prefab;
            case 1011:
                rotation = Quaternion.Euler(0, 180, 0);
                return m_Mountains.Tower3Connectors.prefab;
            case 1100:
                rotation = Quaternion.identity;
                return m_Mountains.Tower2ConnectorsAngle.prefab;
            case 1101:
                rotation = Quaternion.Euler(0, 270, 0);
                return m_Mountains.Tower3Connectors.prefab;
            case 1110:
                rotation = Quaternion.identity;
                return m_Mountains.Tower3Connectors.prefab;
            case 1111:
                rotation = Quaternion.identity;
                return m_Mountains.Tower4Connectors.prefab;
            default:
                rotation = Quaternion.identity;
                return m_Mountains.Tower.prefab;
        }
    }
}