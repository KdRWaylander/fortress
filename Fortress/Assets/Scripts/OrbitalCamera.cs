﻿using UnityEngine;

public class OrbitalCamera : MonoBehaviour
{
    [SerializeField] private Transform m_Pivot;
    [SerializeField] private float m_HorizontalRotationSpeed = 30f;
    [SerializeField] private float m_VerticalRotationSpeed = 10f;
    [SerializeField] private float m_ZoomSpeed = 100f;
    [SerializeField] private float m_MinVerticalAngle = 5f;
    [SerializeField] private float m_MaxVerticalAngle = 75f;
    [SerializeField] private float m_MinZoomDistance = 3f;
    [SerializeField] private float m_MaxZoomDistance = 10f;

    public void UpdateAnchorPosition()
    {
        Vector3 anchorPosition = new Vector3((TerrainManager.Instance.GridWidth - 1) / 2, 0, (TerrainManager.Instance.GridHeight - 1) / 2);
        transform.parent.transform.position = anchorPosition;
    }

    private void FixedUpdate()
    {
        float rot = -Input.GetAxis("Horizontal");
        m_Pivot.Rotate(Vector3.up, Time.fixedDeltaTime * m_HorizontalRotationSpeed * rot, Space.World);

        float vert = transform.localEulerAngles.x < m_MinVerticalAngle && Input.GetAxis("Vertical") <= 0
            || transform.localEulerAngles.x > m_MaxVerticalAngle && Input.GetAxis("Vertical") >= 0 ?
            0:
            Input.GetAxis("Vertical");
        transform.Translate(transform.up * Time.fixedDeltaTime * m_VerticalRotationSpeed * vert, Space.World);

        float depth =
            (transform.position - m_Pivot.position).magnitude < m_MinZoomDistance && Input.GetAxis("Mouse ScrollWheel") >= 0
            || (transform.position - m_Pivot.position).magnitude > m_MaxZoomDistance && Input.GetAxis("Mouse ScrollWheel") <= 0 ?
            0:
            Input.GetAxis("Mouse ScrollWheel");
        transform.Translate(transform.forward * Time.fixedDeltaTime * m_ZoomSpeed * depth, Space.World);

        transform.LookAt(m_Pivot);
    }
}