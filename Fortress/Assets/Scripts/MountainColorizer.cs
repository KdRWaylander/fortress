﻿using UnityEngine;

public class MountainColorizer : MonoBehaviour
{
    [SerializeField] private Color m_GrassColor;

    private void Start()
    {
        MeshRenderer[] meshRenderers = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mr in meshRenderers)
        {
            Material[] materials = mr.materials;
            materials[1].color = m_GrassColor;
            mr.materials = materials;
        }
    }
}