﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UIRestart : MonoBehaviour
{
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}