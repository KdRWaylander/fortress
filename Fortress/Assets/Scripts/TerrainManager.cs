﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainManager : MonoBehaviour
{
    public static TerrainManager Instance { get; private set; }

    public GameObject GroundPrefab { get { return m_GroundPrefab; } }
    public GameObject WaterPrefab { get { return m_WaterPrefab; } }
    public GameObject NaturePrefab { get { return m_NaturePrefab; } }
    public GameObject MountainPrefab { get { return m_MountainPrefab; } }
    public int GridWidth { get { return m_GridWidth; } }
    public int GridHeight { get { return m_GridHeight; } }

    [SerializeField] private GameObject m_TilePrefab;
    [SerializeField] private GameObject m_GroundPrefab;
    [SerializeField] private GameObject m_WaterPrefab;
    [SerializeField] private GameObject m_NaturePrefab;
    [SerializeField] private GameObject m_MountainPrefab;

    [SerializeField] private int m_GridWidth;
    [SerializeField] private int m_GridHeight;

    private Tile[,] m_Tiles;
    private TerrainGenerator m_TerrainGenerator;

    private void Awake()
    {
        Instance = this;

        m_TerrainGenerator = GetComponent<TerrainGenerator>();
    }

    private void Start()
    {
        m_GridWidth = Random.Range(5, 30);
        m_GridHeight = Random.Range(5, 30);
        FindObjectOfType<OrbitalCamera>().UpdateAnchorPosition();

        m_Tiles = new Tile[m_GridWidth, m_GridHeight];
        m_TerrainGenerator.ComputeTextureMap(m_GridWidth, m_GridHeight);

        StartCoroutine(InstantiateTiles(ListCells(m_GridWidth, m_GridHeight)));
    }

    // Returns whether the position (x, z) is inside the grid or not
    public bool PositionIsValid(int x, int z)
    {
        return x >= 0 && x < m_GridWidth && z >= 0 && z < m_GridHeight;
    }

    public bool PositionIsValid(Vector3 position)
    {
        return PositionIsValid((int)position.x, (int)position.z);
    }

    private Queue<Vector3> ListCells(int xMax, int zMax)
    {
        List<Vector3> cellsList = new List<Vector3>();
        Queue<Vector3> cellsQueue = new Queue<Vector3>();

        // Generate all cells' positions
        for (int x = 0; x < xMax; x++)
        {
            for (int z = 0; z < zMax; z++)
            {
                cellsList.Add(new Vector3(x, 0, z));
            }
        }

        // Shuffle the positions for random order instantiation
        cellsList.FisherYatesShuffle();

        // Queue shuffled positions
        for (int i = 0; i < cellsList.Count; i++)
        {
            cellsQueue.Enqueue(cellsList[i]);
        }

        return cellsQueue;
    }

    private IEnumerator InstantiateTiles(Queue<Vector3> positions)
    {
        while (positions.Count > 0)
        {
            Vector3 position = positions.Dequeue();
            int x = (int)position.x;
            int z = (int)position.z;

            // Instantiate and positionate the tile
            m_Tiles[x, z] = Instantiate(m_TilePrefab, position, Quaternion.identity, transform).GetComponent<Tile>();
            m_Tiles[x, z].SetCoordinates(x, z);
            m_Tiles[x, z].SetTileOccupation(m_TerrainGenerator.GetTileOccupation(x, z));

            // set north neighbour and counterpart
            if (PositionIsValid(x, z + 1))
            {
                m_Tiles[x, z].neighbours.north = m_Tiles[x, z + 1];

                if (m_Tiles[x, z + 1] != null)
                    m_Tiles[x, z + 1].neighbours.south = m_Tiles[x, z];
            }
            // no else statement since default value is null

            // set east neighbour and counterpart
            if (PositionIsValid(x + 1, z))
            {
                m_Tiles[x, z].neighbours.east = m_Tiles[x + 1, z];

                if (m_Tiles[x + 1, z] != null)
                    m_Tiles[x + 1, z].neighbours.west = m_Tiles[x, z];
            }

            // set south neighbour and counterpart
            if (PositionIsValid(x, z - 1))
            {
                m_Tiles[x, z].neighbours.south = m_Tiles[x, z - 1];

                if (m_Tiles[x, z - 1] != null)
                    m_Tiles[x, z - 1].neighbours.north = m_Tiles[x, z];
            }

            // set west neighbour and counterpart
            if (PositionIsValid(x - 1, z))
            {
                m_Tiles[x, z].neighbours.west = m_Tiles[x - 1, z];

                if (m_Tiles[x - 1, z] != null)
                    m_Tiles[x - 1, z].neighbours.east = m_Tiles[x, z];
            }

            // Change name
            m_Tiles[x, z].name = "Tile " + (z * m_GridWidth + x);

            // Wait until next frame
            yield return null;
        }
    }
}