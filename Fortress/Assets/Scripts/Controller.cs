﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Controller : MonoBehaviour
{
    private void Update()
    {
        if(EventSystem.current.IsPointerOverGameObject() == false)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                {
                    Tile hitTile;
                    if (hit.transform.tag == "Building")
                    {
                        hitTile = hit.transform.GetComponentInParent<Tile>();
                    }
                    else
                    {
                        hitTile = hit.transform.GetComponent<Tile>();
                    }

                    if (hitTile != null)
                    {
                        BuildManager.Instance.Build(hitTile);
                    }
                }
            }
            else if (Input.GetKeyUp(KeyCode.Mouse1))
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                {
                    Tile hitTile;
                    if (hit.transform.tag == "Building")
                    {
                        hitTile = hit.transform.GetComponentInParent<Tile>();
                    }
                    else
                    {
                        hitTile = hit.transform.GetComponent<Tile>();
                    }

                    if (hitTile != null)
                    {
                        BuildManager.Instance.Erase(hitTile);
                    }
                }
            }
        }
    }
}