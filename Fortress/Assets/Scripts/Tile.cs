﻿using UnityEngine;

public class Tile : MonoBehaviour
{
    public Vector3 position { get { return new Vector3(xGrid, 0, zGrid); } }
    public int xGrid { get; private set; }
    public int zGrid { get; private set; }
    public TileOccupation occupation { get; set; }
    public BuildingName buildingName { get; set; }
    public GameObject ground { get; set; }
    public GameObject top { get; set; }
    public Neighbours neighbours;

    private void Start()
    {
        switch (occupation)
        {
            case TileOccupation.Water:
                occupation = TileOccupation.Water;
                ground = Instantiate(TerrainManager.Instance.WaterPrefab, transform);
                top = null;
                break;
            case TileOccupation.Grass:
                occupation = TileOccupation.Free;
                ground = Instantiate(TerrainManager.Instance.GroundPrefab, transform);
                top = Instantiate(TerrainManager.Instance.NaturePrefab, transform);
                break;
            case TileOccupation.Mountain:
                occupation = TileOccupation.Mountain;
                ground = Instantiate(TerrainManager.Instance.GroundPrefab, transform);
                top = Instantiate(TerrainManager.Instance.MountainPrefab, transform);
                break;
        }
    }

    public void Clear()
    {
        if (top != null) // may be null because of BuildManager.Erase function
        {
            Destroy(top.gameObject);
        }
    }

    public void SetCoordinates(int x, int z)
    {
        xGrid = x;
        zGrid = z;
    }

    public void SetTileOccupation(TileOccupation tileOccupation)
    {
        occupation = tileOccupation;
    }
}

[System.Serializable]
public enum TileOccupation
{
    Water,
    Grass,
    Forest,
    Mountain,
    Free,
    Builded
}

[System.Serializable]
public struct Neighbours
{
    public Tile north;
    public Tile east;
    public Tile south;
    public Tile west;
}