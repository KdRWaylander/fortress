﻿using System.Collections.Generic;
using UnityEngine;

public static class ListExtension
{
    public static List<T> FisherYatesShuffle<T>(this List<T> list)
    {
        T a;
        int j;
        for (int i = list.Count - 1; i > 0; i--)
        {
            j = Random.Range(0, i + 1);
            a = list[i];
            list[i] = list[j];
            list[j] = a;
        }
        return list;
    }
}