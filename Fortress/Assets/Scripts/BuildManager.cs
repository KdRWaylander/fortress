﻿using System;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager Instance { get; private set; }
    public static Buildings CurrentBuildings { get; private set; }

    [SerializeField] private int m_Index = 1;
    [SerializeField] private Buildings[] m_Buildings;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        CurrentBuildings = m_Buildings[m_Index];
    }

    public void Build(Tile tile)
    {

        if(tile.occupation == TileOccupation.Free)
        {
            tile.Clear();
            tile.occupation = TileOccupation.Builded;

            UpdateNeighbour(tile.neighbours.north);
            UpdateNeighbour(tile.neighbours.east);
            UpdateNeighbour(tile.neighbours.south);
            UpdateNeighbour(tile.neighbours.west);

            Vector3 position = new Vector3(Mathf.Round(tile.xGrid), .5f, Mathf.Round(tile.zGrid));
            Quaternion rotation = Quaternion.identity;
            BuildingName buildingName = BuildingName.Tower;
            GameObject building = GetBuildingType(tile.neighbours, out rotation, out buildingName);

            tile.top = Instantiate(building, position, rotation, tile.transform);
            tile.buildingName = buildingName;
        }
        else if (tile.occupation == TileOccupation.Builded)
        {
            if (tile.buildingName == BuildingName.Wall)
            {
                Vector3 position = new Vector3(Mathf.Round(tile.xGrid), .5f, Mathf.Round(tile.zGrid));
                Quaternion rotation = tile.top.transform.rotation;
                BuildingName buildingName = BuildingName.Door;
                GameObject building = CurrentBuildings.Door.prefab;

                // clear only after remembering the wall's rotation
                tile.Clear();

                tile.top = Instantiate(building, position, rotation, tile.transform);
                tile.buildingName = buildingName;
            }
        }
    }

    internal void Erase(Tile tile)
    {
        if ((tile.occupation == TileOccupation.Builded || tile.occupation == TileOccupation.Free) && tile.top != null)
        {
            if(tile.buildingName == BuildingName.Door)
            {
                Vector3 position = new Vector3(Mathf.Round(tile.xGrid), .5f, Mathf.Round(tile.zGrid));
                Quaternion rotation = tile.top.transform.rotation;
                BuildingName buildingName = BuildingName.Wall;
                GameObject building = CurrentBuildings.Wall.prefab;

                // clear only after remembering the wall's rotation
                tile.Clear();

                tile.top = Instantiate(building, position, rotation, tile.transform);
                tile.buildingName = buildingName;
            }
            else
            {
                tile.Clear();
                tile.occupation = TileOccupation.Free;

                UpdateNeighbour(tile.neighbours.north);
                UpdateNeighbour(tile.neighbours.east);
                UpdateNeighbour(tile.neighbours.south);
                UpdateNeighbour(tile.neighbours.west);

                tile.top = null;
                tile.buildingName = BuildingName.None;
            }
        }
    }

    private void UpdateNeighbour(Tile neighbour)
    {
        if(neighbour == null)
            return;

        if (neighbour.occupation == TileOccupation.Builded)
        {
            neighbour.Clear();

            Vector3 position = new Vector3(neighbour.xGrid, .5f, neighbour.zGrid);
            Quaternion rotation = Quaternion.identity;
            BuildingName buildingName = BuildingName.Tower;
            GameObject building = GetBuildingType(neighbour.neighbours, out rotation, out buildingName);

            neighbour.top = Instantiate(building, position, rotation, neighbour.transform);
            neighbour.buildingName = buildingName;
        }
    }

    private static GameObject GetBuildingType(Neighbours neighbours, out Quaternion rotation, out BuildingName buildingName)
    {
        int configuration = 0;
        if (neighbours.north != null && neighbours.north.occupation == TileOccupation.Builded)
            configuration += 1000;
        if (neighbours.east != null && neighbours.east.occupation == TileOccupation.Builded)
            configuration += 100;
        if (neighbours.south != null && neighbours.south.occupation == TileOccupation.Builded)
            configuration += 10;
        if (neighbours.west != null && neighbours.west.occupation == TileOccupation.Builded)
            configuration += 1;

        switch (configuration)
        {
            case 0:
                rotation = Quaternion.Euler(0, 0, 0);
                buildingName = BuildingName.Tower;
                return CurrentBuildings.Tower.prefab;
            case 1:
                rotation = Quaternion.Euler(0, 270, 0);
                buildingName = BuildingName.Tower1Connector;
                return CurrentBuildings.Tower1Connector.prefab;
            case 10:
                rotation = Quaternion.Euler(0, 180, 0);
                buildingName = BuildingName.Tower1Connector;
                return CurrentBuildings.Tower1Connector.prefab;
            case 11:
                rotation = Quaternion.Euler(0, 180, 0);
                buildingName = BuildingName.Tower2ConnectorsAngle;
                return CurrentBuildings.Tower2ConnectorsAngle.prefab;
            case 100:
                rotation = Quaternion.Euler(0, 90, 0);
                buildingName = BuildingName.Tower1Connector;
                return CurrentBuildings.Tower1Connector.prefab;
            case 101:
                rotation = Quaternion.Euler(0, 90, 0);
                buildingName = BuildingName.Wall;
                return CurrentBuildings.Wall.prefab;
            case 110:
                rotation = Quaternion.Euler(0, 90, 0);
                buildingName = BuildingName.Tower2ConnectorsAngle;
                return CurrentBuildings.Tower2ConnectorsAngle.prefab;
            case 111:
                rotation = Quaternion.Euler(0, 90, 0);
                buildingName = BuildingName.Tower3Connectors;
                return CurrentBuildings.Tower3Connectors.prefab;
            case 1000:
                rotation = Quaternion.Euler(0, 0, 0);
                buildingName = BuildingName.Tower1Connector;
                return CurrentBuildings.Tower1Connector.prefab;
            case 1001:
                rotation = Quaternion.Euler(0, 270, 0);
                buildingName = BuildingName.Tower2ConnectorsAngle;
                return CurrentBuildings.Tower2ConnectorsAngle.prefab;
            case 1010:
                rotation = Quaternion.Euler(0, 0, 0);
                buildingName = BuildingName.Wall;
                return CurrentBuildings.Wall.prefab;
            case 1011:
                rotation = Quaternion.Euler(0, 180, 0);
                buildingName = BuildingName.Tower3Connectors;
                return CurrentBuildings.Tower3Connectors.prefab;
            case 1100:
                rotation = Quaternion.Euler(0, 0, 0);
                buildingName = BuildingName.Tower2ConnectorsAngle;
                return CurrentBuildings.Tower2ConnectorsAngle.prefab;
            case 1101:
                rotation = Quaternion.Euler(0, 270, 0);
                buildingName = BuildingName.Tower3Connectors;
                return CurrentBuildings.Tower3Connectors.prefab;
            case 1110:
                rotation = Quaternion.Euler(0, 0, 0);
                buildingName = BuildingName.Tower3Connectors;
                return CurrentBuildings.Tower3Connectors.prefab;
            case 1111:
                rotation = Quaternion.Euler(0, 0, 0);
                buildingName = BuildingName.Tower4Connectors;
                return CurrentBuildings.Tower4Connectors.prefab;
            default:
                rotation = Quaternion.Euler(0, 0, 0);
                buildingName = BuildingName.Tower;
                return CurrentBuildings.Tower.prefab;
        }
    }
}

[System.Serializable]
public struct BuildingType
{
    public BuildingName name;
    public GameObject prefab;
}

[System.Serializable]
public enum BuildingName
{
    Wall,
    Tower,
    Tower1Connector,
    Tower2ConnectorsStraight,
    Tower2ConnectorsAngle,
    Tower3Connectors,
    Tower4Connectors,
    Door,
    None
}