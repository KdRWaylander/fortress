﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugPerlinMaps : MonoBehaviour
{
    [SerializeField] private Image m_GeneralMap;
    [SerializeField] private Image m_ForestMap;

    public void ShowMap(Sprite sprite, Map map)
    {
        if (map == Map.General)
        {
            m_GeneralMap.gameObject.SetActive(true);
            m_GeneralMap.sprite = sprite;
        }
        else
        {
            m_ForestMap.gameObject.SetActive(true);
            m_ForestMap.sprite = sprite;
        }
    }
}

[System.Serializable]
public enum Map
{
    General,
    Forest
}