﻿using UnityEngine;

public class Colorizer : MonoBehaviour
{
    [SerializeField] Gradient m_ColorGradient;

    private void Start()
    {
        GetComponent<MeshRenderer>().material.color = m_ColorGradient.Evaluate(Random.value);
    }
}