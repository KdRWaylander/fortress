﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nature : MonoBehaviour
{
    [SerializeField] private Prop[] m_GrassProps;
    [SerializeField] private Prop[] m_ForestProps;

    private Tile m_Tile;
    private TerrainGenerator m_TerrainGenerator;

    private void Awake()
    {
        m_Tile = transform.parent.GetComponent<Tile>();
        m_TerrainGenerator = FindObjectOfType<TerrainGenerator>();
    }

    private void Start()
    {
        List<PropLocation> propLocations = m_TerrainGenerator.GetPropLocations(m_Tile.xGrid, m_Tile.zGrid, Random.Range(4, 10), .25f);
        StartCoroutine(PopulateTile(propLocations));
    }

    private IEnumerator PopulateTile(List<PropLocation> propLocations)
    {
        foreach (PropLocation pL in propLocations)
        {
            GameObject prop = pL.occupation == TileOccupation.Grass ?
                Instantiate(m_GrassProps[WeightedGameobjectChoice(m_GrassProps)].prefab, transform) :
                Instantiate(m_ForestProps[WeightedGameobjectChoice(m_ForestProps)].prefab, transform);
            prop.transform.localPosition = pL.position;
            prop.transform.localRotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
            float newScale = (Random.value - .5f) / 10f;
            prop.transform.localScale += new Vector3(newScale, newScale, newScale);

            yield return null;
        }
    }

    private int WeightedGameobjectChoice(Prop[] props)
    {
        int index = 0;

        // List of the cumulative sum of weights
        float[] sumOfChances = new float[props.Length];

        // Fill the list + handling the case with only 1 weight
        sumOfChances[0] = props[0].density;
        if (props.Length > 1)
        {
            for (int i = 1; i < props.Length; i++)
            {
                sumOfChances[i] = sumOfChances[i - 1] + props[i].density;
            }

            // Pick a random number between 0 and the total sum of weights
            float r = Random.value * sumOfChances[props.Length - 1];

            // Find the index of the last item whose cumulative sum is above the random number
            // Pick the asset at index and return it
            for (int i = 0; i < props.Length; i++)
            {
                if (r <= sumOfChances[i])
                {
                    index = i;
                    break;
                }
            }
        }
        else // In case there is only 1 item in the list
        {
            index = 0;
        }

        return index;
    }
}

[System.Serializable]
public struct Prop
{
    public GameObject prefab;
    public float density;
};

public struct PropLocation
{
    public Vector3 position;
    public TileOccupation occupation;

    public PropLocation(Vector3 position, TileOccupation occupation)
    {
        this.position = position;
        this.occupation = occupation;
    }
}